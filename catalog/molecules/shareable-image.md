This component is used to show an image with a share icon, and a optional description.

```html
responsive: true
showSource: true
---
<figure class="vf-image-with-icon">
  <img class="vf-image-with-icon__image" src="/img/example-article/kraftverk.jpg" alt="Example image" />
  <figcaption class="vf-image-with-icon__caption">
    <a class="vf-image-with-icon__icon" href="#">Share</a>
    <a class="vf-image-with-icon__description" href="#">2 x 260 MW units</a>
  </figcaption>
</figure>
```

### Dimmed version

Sometimes when you work with images with brighter colors you might need to add the modifier class `.vf-image-with-icon--dimmed` to make the text more readable.

```html
responsive: true
---
<figure class="vf-image-with-icon vf-image-with-icon--dimmed">
  <img class="vf-image-with-icon__image" src="/img/example-article/kraftverk.jpg" alt="Example image" />
  <figcaption class="vf-image-with-icon__caption">
    <a class="vf-image-with-icon__icon" href="#">Share</a>
    <a class="vf-image-with-icon__description" href="#">2 x 260 MW units</a>
  </figcaption>
</figure>
```
