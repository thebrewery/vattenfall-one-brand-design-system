A bunch of different buttons.

### Primary buttons / Large

```html
showSource: true
---
<div class="vf-row">
  <div class="vf-col">
    <button type="button" class="vf-btn vf-btn--lg vf-btn--primary">Yellow button</button>
  </div>
  <div class="vf-col">
    <button type="button" class="vf-btn vf-btn--lg vf-btn--secondary">Blue Button</button>
  </div>
  <div class="vf-col">
    <button type="button" class="vf-btn vf-btn--lg vf-btn--tertiary">Tertiary Button</button>
  </div>
</div>
<div class="vf-row" style="padding-top: 20px">
  <div class="vf-col">
    <button type="button" disabled class="vf-btn vf-btn--lg vf-btn--primary">Disabled</button>
  </div>
    <div class="vf-col">
    <button type="button" disabled class="vf-btn vf-btn--lg vf-btn--secondary">Disabled</button>
  </div>
  <div class="vf-col"></div>
</div>
```

### Primary buttons / Medium

```html
showSource: true
---
<div class="vf-row">
  <div class="vf-col">
    <button type="button" class="vf-btn vf-btn--primary">Yellow button</button>
  </div>
  <div class="vf-col">
    <button type="button" class="vf-btn vf-btn--secondary">Blue Button</button>
  </div>
  <div class="vf-col">
    <button type="button" class="vf-btn vf-btn--tertiary">Tertiary Button</button>
  </div>
</div>
<div class="vf-row" style="padding-top: 20px">
  <div class="vf-col">
    <button type="button" disabled class="vf-btn vf-btn--primary">Disabled</button>
  </div>
  <div class="vf-col">
    <button type="button" disabled class="vf-btn vf-btn--secondary">Disabled</button>
  </div>
  <div class="vf-col"></div>
</div>
```

### Outlined buttons / Large

```html
showSource: true
---
<div class="vf-row">
  <div class="vf-col">
    <button type="button" class="vf-btn vf-btn--lg vf-btn--outline-dark">Dark button</button>
  </div>
  <div class="vf-col">
    <button type="button" class="vf-btn vf-btn--lg vf-btn--outline-secondary">Blue Button</button>
  </div>
</div>
<div class="vf-row" style="padding-top: 20px">
  <div class="vf-col">
    <button type="button" disabled class="vf-btn vf-btn--lg vf-btn--outline-dark">Disabled</button>
  </div>
  <div class="vf-col">
    <button type="button" disabled class="vf-btn vf-btn--lg vf-btn--outline-secondary">Disabled</button>
  </div>
</div>
```

### Outlined buttons / Medium

```html
showSource: true
---
<div class="vf-row">
  <div class="vf-col">
    <button type="button" class="vf-btn  vf-btn--outline-dark">Dark button</button>
  </div>
  <div class="vf-col">
    <button type="button" class="vf-btn  vf-btn--outline-secondary">Blue Button</button>
  </div>
</div>
<div class="vf-row" style="padding-top: 20px">
  <div class="vf-col">
    <button type="button" disabled class="vf-btn  vf-btn--outline-dark">Disabled</button>
  </div>
  <div class="vf-col">
    <button type="button" disabled class="vf-btn  vf-btn--outline-secondary">Disabled</button>
  </div>
</div>
```

### Text Button

```html
showSource: true
---
<div class="vf-row">
  <div class="vf-col">
    <a href="#" class="vf-link-with-arrow">Text Button</a>
  </div>
  <div class="vf-col">
  </div>
</div>
```

### Text Button / Small

```html
showSource: true
---
<div class="vf-row">
  <div class="vf-col">
    <a href="#" class="vf-link-with-arrow vf-link-with-arrow--small">Text Button - smaller</a>
  </div>
  <div class="vf-col">
  </div>
</div>
```

### Button combinations

Coming soon

### Full width buttons

Create block level buttons—those that span the full width of a parent—by adding .vf-btn--block.

```html
showSource: true
---
<button type="button" class="vf-btn vf-btn--primary vf-btn--block">Block level button</button>
<button type="button" class="vf-btn vf-btn--secondary vf-btn--block">Block level button</button>
```

### Active state
Buttons will appear pressed (with a darker background, darker border, and inset shadow) when active. There’s no need to add a class to <button>s as they use a pseudo-class. However, you can still force the same active appearance with .active (and include the aria-pressed="true" attribute) should you need to replicate the state programmatically.

```html
showSource: true
---
<a href="#" class="vf-btn vf-btn--primary vf-btn--lg active" role="button" aria-pressed="true">Primary link</a>
<a href="#" class="vf-btn vf-btn--secondary vf-btn--lg active" role="button" aria-pressed="true">Link</a>
```