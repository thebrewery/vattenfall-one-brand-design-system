Simply add one of the below classes to reflect the state.

### Error

```html
showSource: true
plain: false
---

<span class="vf-utility-typo--error">Error</span>

  
```

### Link

```html
showSource: true
plain: false
---

<span class="vf-utility-typo--link">Link</span>

  
```

### Metadata

```html
showSource: true
plain: false
---

<span class="vf-utility-typo--metadata">Metadata</span>

  
```

### Success

```html
showSource: true
plain: false
---

<span class="vf-utility-typo--success">Success</span>

  
```

### Warning

```html
showSource: true
plain: false
---

<span class="vf-utility-typo--warning">Warning</span>

  
```


