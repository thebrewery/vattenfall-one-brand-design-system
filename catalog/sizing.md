### Width classes

Available classes:
.w-25
.w-50
.w-75
.w-100
.w-auto

```html
showSource: true
---
<div class="w-25 p-3" style="background-color: #EDF1F6;">Width 25%</div>
<div class="w-50 p-3" style="background-color: #EDF1F6;">Width 50%</div>
<div class="w-75 p-3" style="background-color: #EDF1F6;">Width 75%</div>
<div class="w-100 p-3" style="background-color: #EDF1F6;">Width 100%</div>
<div class="w-auto p-3" style="background-color: #EDF1F6;">Width auto</div>
```

You can also use class `.mw-100` to achieve `max-width: 100%;`.

```html
showSource: true
---
<img class="mw-100" data-src="holder.js/1000px100?text=Max-width%20%3D%20100%25" alt="Max-width 100%">
```
### Height classes

```html
showSource: true
---
<div style="height: 100px; background-color: rgba(255,254,229,0.8);">
  <div class="h-25 d-inline-block" style="width: 120px; background-color: rgba(237,241,2461)">Height 25%</div>
  <div class="h-50 d-inline-block" style="width: 120px; background-color: rgba(237,241,2461)">Height 50%</div>
  <div class="h-75 d-inline-block" style="width: 120px; background-color: rgba(237,241,2461)">Height 75%</div>
  <div class="h-100 d-inline-block" style="width: 120px; background-color: rgba(237,241,2461)">Height 100%</div>
  <div class="h-auto d-inline-block" style="width: 120px; background-color: rgba(237,241,2461)">Height auto</div>
</div>
```

You can also use class `.mh-100` to achieve `max-height: 100%;`.

```html
showSource: true
---
<div style="height: 100px; background-color: rgba(255,254,229,0.8);">
  <div class="mh-100" style="width: 100px; height: 200px; background-color: rgba(237,241,246,1);">Max-height 100%</div>
</div>
```

## VH clases

These classes for using percentages of the current window height are available:

`.vf-vh-100`

`.vf-vh-80`

`.vf-vh-60`

`.vf-vh-50`