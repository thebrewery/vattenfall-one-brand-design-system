## Get started / Designers
In order to get started with Vattenfall Digital Design System, make sure you have been provided with both apps: Abstract and Sketch. Abstract is a version control tool, which keeps all of your Sketch working files in one place. It also prevents creating conflicting copies and allows contributors to request reviews and inspect the files without opening Sketch.

__Abstract introduction__
__Sketch Introduction__
__Sketch + Abstract collaboration__


### Vattenfall Design Kit
The Vattenfall Design Kit (VDK) is a living library that contains Vattenfall's digital visual assets for everyday purpose: modules, components, icons, color palettes, grids, typography, etc. The purpose of VDK is to ensure design and development consistency across entire Vattenfall ecosystem, improve quality & efficiency in production, bring digital products & services to next level of coherency. The VDK can be accessed via Abstract in Sketch format and connected to your working design documents as a Sketch Library source.


Vattenfall Design Kit on Abstract
Vattenfall Design Kit on Github

### Design Kit Structure
Have a look at the Design Kit structure here. It is a nested library of symbols for all everyday design needs. It is grouped by a type of module or component with different aspects depending on the use case of such.


The library of Design Kit contains more that 500 ready-to-use design snippets: from simply buttons to more complex modules. Make sure to study the component library as well and use components with correct naming.

We use a simplified version of the Atomic Design methodology, which we have adapted to suit our design process and our team culture. The main difference with Atomic Design is that all modules are either an atom or a molecule, categorised by purpose. Having organisms used to cause confusion in the team, so we removed them.
Our approach to defining modules and naming them is function directed, rather than presentational. It is based on evolving a shared design language collaboratively and empowering all designers and engineers on the team to contribute to the system.
