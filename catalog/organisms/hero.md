The Hero component is used for large areas in top of pages to convey a message or describe what the page is about.

### Basic example

```html
responsive: true
showSource: true
---
<section 
  class="vf-hero"
  style="background-image: url(/img/example-startpage/hero-beach.jpg);"
  >
  <div class="vf-hero__content">
    <h1 class="vf-hero__headline">
      Fossil-free within <br class="vf-u--sm-hidden" />
      one generation
    </h1>
    <a href="#" class="">
      <img src="/img/example-startpage/play.svg" width="14" style="margin-right: 8px" />
      Watch now
    </a>
  </div>
</section>
```
