The default Favicon looks like this.

```image
light: true
span: 2
src: "/favicon/android-chrome-512x512.png"
```


# How to add a favicon

Use one or more of these lines to add a favion for your applications use case.

```
<link rel="apple-touch-icon" sizes="180x180" href="/path/to/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/path/to/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/path/to/favicon-16x16.png">
<link rel="manifest" href="/path/to/site.webmanifest">
<link rel="shortcut icon" href="/path/to/favicon.ico">
```

# Assets

Different common sizes for download below

```download
title: Favicon 48x48px ICO
url: "/favicon/favicon.ico"
```

```download|span-3
title: Favicon 16x16px PNG
url: "/favicon/favicon-16x16.png"
```

```download|span-3
title: Favicon 32x32px PNG
url: "/favicon/favicon-32x32.png"
```

```download|span-3
title: Android Chrome 512x512px PNG
url: "/favicon/android-chrome-512x512.png"
```

```download|span-3
title: Android Chrome 192x192px PNG
url: "/favicon/android-chrome-192x192.png"
```

```download
title: Apple Touch Icon 180x180px PNG
url: "/favicon/apple-touch-icon.png"
```
