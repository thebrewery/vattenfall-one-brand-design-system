Several icons are availble as fonts. This list will be continually expanded upon.

# How to add an icon

Simply add one of the below classes to the element:

* `vf-icon-placeholder`
* `vf-icon-settings`
* `vf-icon-up`
* `vf-icon-down`
* `vf-icon-left`
* `vf-icon-right`
* `vf-icon-question`

# Examples

```html
showSource: true
---
<div class="vf-container">

	<div class="vf-icon-placeholder">
		Placeholder
	</div>

	<div class="vf-icon-settings">
		Settings
	</div>

	<div class="vf-icon-up">
		Up
	</div>

	<div class="vf-icon-down">
		Down
	</div>

	<div class="vf-icon-left">
		Left
	</div>

	<div class="vf-icon-right">
		Right
	</div>

	<div class="vf-icon-question">
		Question
	</div>

</div>

```