import Examples from "../EXAMPLES";

const nav = {
  title: "Examples",
  path: "/examples",
  content: Examples
}

export default nav